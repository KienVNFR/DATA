from pickletools import optimize
from turtle import forward
import torch 
from torch import nn 
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
from Testmodel import NeuralNetwork

#training data loader 
training_data = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
)

test_data = datasets.FashionMNIST(
    root="data",
    train=False,
    download=True,
    transform=ToTensor(),
)
# create batch train 
batch_size=64
# load data to train and load data to test 
traindata_loader=DataLoader(training_data,batch_size=batch_size)
testdata_loadder=DataLoader(test_data,batch_size=batch_size)
# for X,Y in testdata_loadder:
# 	print(X.shape)
# 	print(Y.shape,Y.dtype)

#Create device for train 
device="cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

model = NeuralNetwork().to(device)

#test segment 
# X=torch.rand(1,28,28,device=device)
# logits=model(X)
# pred_probab = nn.Softmax(dim=1)(logits)
# y_pred = pred_probab.argmax(1)
# print(f"logits here: {logits}")
# print(f"Predicted class: {y_pred}")

#optimizing 
loss_fn=nn.CrossEntropyLoss()
optimizer=torch.optim.SGD(model.parameters(),lr=1e-3)

def train(traindata_loader, model, loss_fn, optimizer):
    size = len(traindata_loader.dataset)
    model.train()
    for batch, (X, y) in enumerate(traindata_loader):
        X, y = X.to(device), y.to(device)

        # Compute prediction error
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test(testdata_loadder, model, loss_fn):
    size = len(testdata_loadder.dataset)
    num_batches = len(testdata_loadder)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in testdata_loadder:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

epochs = 5
for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    train(traindata_loader, model, loss_fn, optimizer)
    test(testdata_loadder, model, loss_fn)
print("Done!")

torch.save(model.state_dict(), "model.pth")
print("Saved PyTorch Model State to model.pth")